package net.fabricmc.togglescoreboard.mixin;

import net.fabricmc.togglescoreboard.Values;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import me.shedaniel.autoconfig.AutoConfig;
import org.spongepowered.asm.mixin.injection.At;
import net.minecraft.client.gui.hud.InGameHud;

@Mixin(InGameHud.class)
public class MixinInGameHud {
    Values config = AutoConfig.getConfigHolder(Values.class).getConfig();
    @Inject(method = "renderScoreboardSidebar", at = @At("HEAD"), cancellable = true)
    protected void $toggleScoreboardOnRenderScoreboardSidebar(CallbackInfo ci) {
        if (!config.RENDER_BOARD) {
            ci.cancel();
        }
    }
}
