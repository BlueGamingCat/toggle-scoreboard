This is a simple mod that adds a keybinding to toggle the side scoreboard.

Go to `Options` => `Controls` => `Key Binds` and locate the `Toggle Scoreboard` category to set a key bind.

Once set you should be able to simply use the new key bind to toggle the scoreboard on and off.

I use all my mods on https://alinea.gg/ so feel free to check it out ^^
