package us.mikebartosh.minecraft.togglescoreboard;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;

@Config(name = "toggle-scoreboard")
public class Values implements ConfigData {
    public Boolean RENDER_BOARD = true;
    public Boolean RENDERED_BOARD = false;
}
