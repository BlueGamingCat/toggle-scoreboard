package us.mikebartosh.minecraft.togglescoreboard;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.Toml4jConfigSerializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;

public class ToggleScoreboard implements ModInitializer {
	KeyBinding scoreboardToggle = KeyBindingHelper.registerKeyBinding(new KeyBinding("Scoreboard Toggle", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Scoreboard"));
	public Values config = null;

	@Override
	public void onInitialize() {
		ConfigHolder<Values> autoConfig = AutoConfig.register(Values.class, Toml4jConfigSerializer::new);
		Values config = AutoConfig.getConfigHolder(Values.class).getConfig();

		ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (scoreboardToggle.isPressed()) {
				if (!config.RENDERED_BOARD) {
					config.RENDER_BOARD = !config.RENDER_BOARD;
				}
				config.RENDERED_BOARD = true;

				autoConfig.save();
			} else if (config.RENDERED_BOARD) {
				config.RENDERED_BOARD = false;

				autoConfig.save();
			}
			// The reason I am checking if RENDERED_BOARD is true is so I don't save the config every tick.
        });
	}
}
